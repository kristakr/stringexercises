public class Ex1Presidents {
    //Ülesanne 1:
    //● Defineeri muutujad
    //○ president1 ​väärtusega “Konstantin Päts”
    //○ president2 ​väärtusega “Lennart Meri”
    //○ president3 ​väärtusega “Arnold Rüütel”
    //○ president4 ​väärtusega “Toomas Hendrik Ilves”
    //○ president5 ​väärtusega “Kersti Kaljulaid”
    //
    //● Defineeri String-tüüpi muutuja ja pane see viitama järgmisele
    //tekstile: “Konstantin Päts, Lennart Meri, Arnold Rüütel,
    //Toomas Hendrik Ilves ja Kersti Kaljulaid on Eesti
    //presidendid.”​. Kasuta lause konstrueerimiseks StringBuilder
    //klassi.
    //● Prindi tekst standardväljundisse.
    public static void main(String[] args) {
        String president1 = "Konstantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";
        StringBuilder stringBuilder = new StringBuilder("\"").append(president1).append(", ")
                .append(president2).append(", ").append(president3).append(", ")
                .append(president4).append(" ja ").append(president5).append( " on Eesti presidendid\"");
        System.out.println(stringBuilder);
        String wholeText = stringBuilder.toString();
        System.out.println(wholeText);
    }
}
