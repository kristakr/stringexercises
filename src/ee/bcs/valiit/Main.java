package ee.bcs.valiit;
//Prindi standardväljundisse järgmised tekstiread:
//● Hello, World!
//● Hello “World”!
//● Steven Hawking once said: “Life would be tragic if it weren’t
//funny”.
//● Kui liita kokku sõned “See on teksti esimene pool ” ning “See
//on teksti teine pool”, siis tulemuseks saame “See on teksti
//esimene pool See on teksti teine pool”.
//● Elu on ilus.
//● Elu on ‘ilus’.
//● Elu on “ilus”.
//● Kõige rohkem segadust tekitab “-märgi kasutamine sõne sees.
//● Eesti keele kõige ilusam lause on: “Sõida tasa üle silla!”
//● ‘Kolm’ - kolm, ‘neli’ - neli, “viis” - viis.
public class Main {

    public static void main(String[] args) {
	// write your code here

        System.out.println("Hello, World!");
        System.out.println("Hello /World/*!");
        System.out.println("Steven Hawking once said: “Life would be tragic if it weren’t funny”.");
        String str1 = "See on teksti esimene pool";
        String str2 = "See on teksti teine pool";
        String wholeText =str1 + " " + str2;
        //System.out.println("Kui liita kokku sõned " + str1 + " ja " + str2+ ", siis kokku saame " + wholeText);
        StringBuilder stringBuilder = new StringBuilder("Kui kokku liita sõned ").
                append("\"").append(str1).append("\"").append(" ja ").
                append("\"").append(str2).append("\"").append(", siis kokku saame ").append("\"").
                append(wholeText).append("\"").append(".");
        System.out.println(stringBuilder);
        System.out.println("Elu on ilus.");
        System.out.println("Elu on ‘ilus’.");
        System.out.println("Elu on “ilus”.");
        System.out.println("Elu on \"ilus\".");
        System.out.println("Elu on \'ilus\'.");
        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"");
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.");


    }
}
