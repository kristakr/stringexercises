public class Ex3Planets {
    //Ülesanne 3:
    //● Defineeri muutujad
    //○ planet1 väärtusega “Merkuur”
    //○ planet2 väärtusega “Venus”
    //○ planet3 väärtusega “Maa”
    //○ planet4 väärtusega “Marss”
    //○ planet5 väärtusega “Jupiter”
    //○ planet6 väärtusega “Saturn”
    //○ planet7 väärtusega “Uran”
    //○ planet8 väärtusega “Neptuun”
    //○ planetCount väärtusega 8
    //● Prindi standardväljundisse lause: “Merkuur, Veenus, Maa,
    //Marss, Jupiter, Saturn, Uraan ja Neptuun on Päikesesüsteemi 8
    //planeeti” , kasutades eelnevalt defineeritud muutujaid.
    //● Prindi standardväljundisse sama lause, aga kasuta seejuures
    //abifunktsiooni String.format() .
    public static void main(String[] args) {
        String planet1 = "Merkuur";
        String planet2 = "Venus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uran";
        String planet8 = "Neptuun";
        int planetCount = 8;
        System.out.println( "\"" + planet1 + ", " + planet2 + ", " + planet3 + ", " + planet4 + ", " + planet5 + ", " + planet6 + ", " + planet7 + " ja " + planet8 + " on Päikesesüsteemi " + planetCount + " planeeti\"" );
        //System.out.println(String.format("Hello %s  %s!", args[0],args[1] ));
        System.out.println(String.format("\"%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %s planeeti\"",
                planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount));
    }


}
