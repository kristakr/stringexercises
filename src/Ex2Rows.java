import java.util.Scanner;

public class Ex2Rows {
    //Ülesanne 2:
    //● Defineeri String-tüüpi muutuja ja pane see viitama tekstile
    //“Rida: See on esimene rida. Rida: See on teine rida. Rida:
    //See on kolmas rida.”
    //● Kasutades Scanner klassi, prindi standardväljundisse
    //järgmised read:
    //See on esimene rida.
    //See on teine rida.
    //See on kolmas rida.
    public static void main(String[] args) {
        String myString = "See on esimene rida.\nSee on teine rida.\nSee on kolmas rida.";
        System.out.println(myString);
        System.out.println("-----------");
        Scanner scanner2 = new Scanner(myString);
        while (scanner2.hasNextLine()) {
            String line = scanner2.nextLine();
            System.out.println(line);
        }
         scanner2.close();

            // process the line
        System.out.println("Enter your username: ");
        Scanner scanner = new Scanner(System.in);
        String username = scanner.nextLine();
        System.out.println("Your username is " + username);
        scanner.close();



    }
}
