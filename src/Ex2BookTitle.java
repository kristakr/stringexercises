public class Ex2BookTitle {
    //Ülesanne 2:
    //● Defineeri muutuja bookTitle .
    //● Omista muutujale väärtus “Rehepapp”
    //● Prindi standardväljundisse tekst: “Raamatu “Rehepapp” autor
    //on Andrus Kivirähk” , kus raamatu nimi pärineb muutujast
    //bookTitle .
    public static void main(String[] args) {
        String bookTitle = "Rehepapp";
        System.out.println("\"Raamatu \"" + bookTitle+ "\" autor on Andrus Kivirähk\"");
    }
}
